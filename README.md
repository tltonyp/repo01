# PROJECT OWNER #

Tien-Loc Tony Pham

Created for INFO2300

- - -

# Install Project #
Installing this project is simple.

1.Click [Source](https://bitbucket.org/tltonyp/repo01/src/master/) on the left side

2.Download the [Index.html](https://bitbucket.org/tltonyp/repo01/src/master/index.html)

3.Open the file in your prefered browser.

- - - 
# Licence Information #

MIT License

Copyright (c) 2022 Tien-Loc Tony Pham

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

I chose this license because it is very open and generic, which fits well with the purpose of this repository.